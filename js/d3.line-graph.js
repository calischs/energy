//make a stacked line graph from data
//following chart API, except using selection as input: https://bost.ocks.org/mike/chart/
function lineGraph() {
  var width = 600, // default width
      height = 400, // default height
      margin = {top: 30, right: 20, bottom: 30, left: 50},
      color = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]),
      xaxislabel = '',
      yaxislabel = '',
      offset = 'zero',
      selection_string = '',
      legend_font_size="",
      title = '',
      data = []; //data

  function my() {

    //create axes
    var minyval = 0;//d3.min(data, function(d){return d3.min(_.map(d.values, 'y'));});
    var maxyval = d3.max(data, function(d){return d3.max(_.map(d.values, 'y'));});
    var minxval = d3.min(data, function(d){return d3.min(_.map(d.values, 'x'));});
    var maxxval = d3.max(data, function(d){return d3.max(_.map(d.values, 'x'));});
    
    var x = d3.scale.linear().domain([minxval,maxxval]).range([0, width]);
    var y = d3.scale.linear().domain([minyval,maxyval]).range([height, 0]);
    var xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(10);
    var yAxis = d3.svg.axis().scale(y).orient("left").ticks(10);

    var line = d3.svg.line()
    .x(function(d) { return x(d.x); })
    .y(function(d) { return y(d.y); });

    //create svg
    var svg = d3.select(selection_string)
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .attr("class","graph")
        .append("g")
            .attr("transform", 
                  "translate(" + margin.left + "," + margin.top + ")");

    svg.selectAll("path")
        .data(_.map(data, 'values'))
        .enter()
          .append("path")
          .attr("fill","none")
          .attr("d", line)
          .attr("stroke-width","2px")
          .style("stroke", function(d,i) { return color(i); })
          .attr("data-legend",function(d,i) { return data[i].name});

    //must append axes after binding data.
      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis)
          .style("font-size",12);
      svg.append("g")
          .attr("class", "y axis")
          .call(yAxis)
          .style("font-size",12);

        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", "translate("+ (margin.left/2) +","+(height/2)+")rotate(-90)")
            .text(yaxislabel);

        svg.append("text")
            .attr("text-anchor", "middle")  
            .attr("transform", "translate("+ (width/2) +","+(height-(margin.bottom/3))+")")
            .text(xaxislabel);

    if (legend_font_size != 0){
      legend = svg.append("g")
        .attr("class","legend")
        .attr("transform","translate(50,50)")
        .style("font-size",legend_font_size)
        .call(d3.legend)
    }

    if (title != ''){
      svg.append("text")
              .attr("x", width/2)             
              .attr("y", 16)
              .attr("text-anchor", "middle")  
              .style("font-size", "16px") 
              .text(title);
    }

  }

  my.width = function(value) {
    if (!arguments.length) return width;
    width = value;
    return my;
  };

  my.height = function(value) {
    if (!arguments.length) return height;
    height = value;
    return my;
  };
  my.margin = function(value) {
    if (!arguments.length) return margin;
    margin = value;
    return my;
  };
  my.color = function(value) {
    if (!arguments.length) return color;
    color = value;
    return my;
  };
  my.xaxislabel = function(value) {
    if (!arguments.length) return xaxislabel;
    xaxislabel = value;
    return my;
  };
  my.yaxislabel = function(value) {
    if (!arguments.length) return yaxislabel;
    yaxislabel = value;
    return my;
  };
  my.offset = function(value) {
    if (!arguments.length) return offset;
    offset = value;
    return my;
  };
  my.legend_font_size = function(value) {
    if (!arguments.length) return legend_font_size;
    legend_font_size = value;
    return my;
  };
  my.title = function(value) {
    if (!arguments.length) return title;
    title = value;
    return my;
  };
  my.selection_string = function(value) {
    if (!arguments.length) return selection_string;
    selection_string = value;
    return my;
  };
  my.data = function(value) {
    if (!arguments.length) return data;
    data = value;
    return my;
  };



  return my;
}