function add_entry(biblist,citation){
	var entry = document.createElement("li");
	if (citation.entryType == 'article' && 'Url' in citation){
		var template = "\
			<div>\
				<span id='citation_number'>[{{citation_number}}] </span>\
					<b>{{Author}}</b> ({{Year}}) <a href='{{Url}}' />{{Title}}.</a> {{Journal}}, {{Volume}}\
			</div>";
		var html = Mustache.to_html(template,citation);
		entry.innerHTML = html;
	} else 	if (citation.entryType == 'book' && 'Url' in citation){
		var template = "\
			<div>\
				<span id='citation_number'>[{{citation_number}}] </span>\
					<b>{{Author}}</b> ({{Year}}) <a href='{{Url}}' />{{Title}}.</a> {{Publisher}}\
			</div>";
		var html = Mustache.to_html(template,citation);
		entry.innerHTML = html;
	} else 	if (citation.entryType == 'techreport' && 'Url' in citation){
		var template = "\
			<div>\
				<span id='citation_number'>[{{citation_number}}] </span>\
					<b>{{Author}}</b> ({{Year}}) <a href='{{Url}}' />{{Title}}.</a> {{Institution}}\
			</div>";
		var html = Mustache.to_html(template,citation);
		entry.innerHTML = html;
	} else 	if (citation.entryType == 'inproceedings' && 'Url' in citation){
		var template = "\
			<div>\
				<span id='citation_number'>[{{citation_number}}] </span>\
					<b>{{Author}}</b> ({{Year}}) <a href='{{Url}}' />{{Title}}.</a> {{Booktitle}}\
			</div>";
		var html = Mustache.to_html(template,citation);
		entry.innerHTML = html;
	}
	biblist.appendChild(entry);

}

function parse_citations(bibfilepath){
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	} else {
	  alert('The File APIs are not fully supported in this browser.');
	}

	//get .bib reference file
	$.get(bibfilepath, function(bibdata) { 
		bibdata = doParse(bibdata);
		//get inline citations
		var citations = document.getElementsByTagName("cite");
		//set up bibliography
		var bib = document.getElementById("bib");
		var biblist = document.createElement("ul");
		bib.appendChild(biblist);
		var already_in_bib = [];
		for(var i=0; i<citations.length; i++){
			if (citations[i].id in bibdata){
				if ( already_in_bib.indexOf(citations[i].id)!=-1 ){
					var citation_index = already_in_bib.indexOf(citations[i].id)+1;
					citations[i].innerHTML = "["+citation_index+"]";
				} else {
					citations[i].innerHTML = "["+(already_in_bib.length+1)+"]"
					bibdata[citations[i].id]["citation_number"] = already_in_bib.length+1;
					add_entry(biblist,bibdata[citations[i].id]);
					already_in_bib.push(citations[i].id);
				}
			} else {
				citations[i].innerHTML = "[?]"
			}
		}


	});
}

function make_all_citations(bibfilepath){
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	} else {
	  alert('The File APIs are not fully supported in this browser.');
	}

	//get .bib reference file
	$.get(bibfilepath, function(bibdata) { 
		bibdata = doParse(bibdata);
		delete bibdata['@comments'];
		//set up bibliography
		var bib = document.getElementById("bib");
		var biblist = document.createElement("ul");
		bib.appendChild(biblist);
		var already_in_bib = [];
		var sorted_keys = Object.keys(bibdata).sort( function(a,b){
			author1 = bibdata[a].Author.toUpperCase();
			author2 = bibdata[b].Author.toUpperCase();
			return (author1 < author2) ? -1 : (author1 > author2) ? 1 : 0;
		} )
		sorted_keys.forEach( function(key){
			console.log(bibdata[key])
			bibdata[key]["citation_number"] = already_in_bib.length+1;
			add_entry(biblist,bibdata[key]);
			already_in_bib.push(key);
		});


	});


}