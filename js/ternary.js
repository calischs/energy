function ternaryPlot(selector, userOpt ) {

	var plot = {
		dataset:[]
	};

	var opt = {
		width:900,
		height:500,
		side: 700,
		margin: {top:50,left:50,bottom:50,right:50},
		axis_labels:['A','B','C'],
		axis_ticks:[0,20,40,60,80,100],
		tickLabelMargin:15,
		circle_radius:4,
		axisLabelMargin:50 }

	for(var o in userOpt){
		opt[o] = userOpt[o];
	}

	var svg = d3.select(selector).append('svg')
						.attr("width", opt.width)
                        .attr("height", opt.height);

	defs = svg.append("defs")

	defs.append("marker")
		.attr({
			"id":"arrow",
			"viewBox":"0 -5 10 10",
			"refX":5,
			"refY":0,
			"style":"fill:#aaa",
			"markerWidth":8,
			"markerHeight":8,
			"orient":"auto"
		})
		.append("path")
			.attr("d", "M0,-5L10,0L0,5L2,0")
			.attr("class","arrowHead");
	var filter = defs.append("filter")
		.attr({
			"id":"solid",
			"x":"0",
			"y":"0",
			"width":"1",
			"height":"1"});

	filter.append("feFlood").attr({
		"flood-color":"#fff",
		"flood-opacity":".8"})
	filter.append("feComposite").attr("in","SourceGraphic")

	var axes = svg.append('g').attr('class','axes');

    var w = opt.side;
    var h = Math.sqrt( opt.side*opt.side - (opt.side/2)*(opt.side/2));

	var corners = [
		[opt.margin.left, h + opt.margin.top], // a
		[ w + opt.margin.left, h + opt.margin.top], //b 
		[(w/2) + opt.margin.left, opt.margin.top] ] //c
	var midpoints = [
		[opt.margin.left+.25*w, .5*h + opt.margin.top], // a
		[ .5*w + opt.margin.left, h + opt.margin.top], //b 
		[.75*w + opt.margin.left, .5*h + opt.margin.top] ] //c
	//axis names
	var axislabel = axes.selectAll('.axis-title')
		.data(opt.axis_labels)
		.enter()
			.append('g')
				.attr('class', 'axis-title')
				.attr('transform',function(d,i){
					var theta = 0;
					//if(i===0) theta = 120;
					if(i===0) {theta = -150; alpha = -60;}
					if(i===1) {theta = 90; alpha = 0;}
					if(i===2) {theta = -30; alpha = 60;}

					var x = opt.axisLabelMargin * Math.cos(theta * 0.0174532925),
						y = opt.axisLabelMargin * Math.sin(theta * 0.0174532925);
					return 'translate('+(x+midpoints[i][0])+','+(y+midpoints[i][1])+') rotate('+alpha+')';
				});

	axislabel.append('line')
				.attr({
					"class":"arrow",
					"marker-end":"url(#arrow)",
					"y1":-5,
					"y2":-5}
				)
				.attr("x1",function(d,i){return i==1 ? -(opt.side/2-100): opt.side/2-100;})		
				.attr("x2",function(d,i){return i==1 ? opt.side/2-30: -(opt.side/2-30);});	
	axislabel.append('text')
				.text(function(d){ return d; })
				.attr('x',function(d,i){return i==1 ? 100 : -100;})
				.attr('filter','url(#solid)')
				.attr('text-anchor', function(d,i){
					//if(i===1) return 'beginning';
					//if(i===2) return 'middle';
					return 'middle';	
				})
				.attr('alignment-baseline', function(d,i){
					//if(i===1) return 'middle';
					return 'baseline';
				});




	//ticks
	var n = opt.axis_ticks.length;
	if(opt.minor_axis_ticks){
		opt.minor_axis_ticks.forEach(function(v) {	

			var coord1 = coord( [v, 0, 100-v] )
			var coord2 = coord( [v, 100-v, 0] )
			var coord3 = coord( [0, 100-v, v] )
			var coord4 = coord( [100-v, 0, v] )
			axes.append("line")
				.attr( make_line(coord1, coord2, -5,0) )
				.classed('a-axis minor-tick', true);	

			axes.append("line")
				.attr( make_line(coord2, coord3, -5,0)  )
				.classed('b-axis minor-tick', true);	

			axes.append("line")
				.attr( make_line(coord3, coord4, -5,0)  )
				.classed('c-axis minor-tick', true);		
		});
	}

	opt.axis_ticks.forEach(function(v) {	

		var coord1 = coord( [v, 0, 100-v] )
		var coord2 = coord( [v, 100-v, 0] )
		var coord3 = coord( [0, 100-v, v] )
		var coord4 = coord( [100-v, 0, v] )
		axes.append("line")
			.attr( make_line(coord1, coord2, -10,0)  )
			.classed('a-axis tick', true);	

		axes.append("line")
			.attr( make_line(coord2, coord3, -10,0)  )
			.classed('b-axis tick', true);	

		axes.append("line")
			.attr( make_line(coord3, coord4, -10,0)  )
			.classed('c-axis tick', true);	


		//tick labels
		axes.append('g')
			.attr('transform',function(d){
				return 'translate(' + coord1[0] + ',' + coord1[1] + ')'
			})
			.append("text")
				.attr('transform','rotate(60)')
				.attr('text-anchor','end')
				.attr('x',-opt.tickLabelMargin)
				.text( function (d) { return v; } )
				.classed('a-axis tick-text', true );

		axes.append('g')
			.attr('transform',function(d){
				return 'translate(' + coord2[0] + ',' + coord2[1] + ')'
			})
			.append("text")
				.attr('transform','rotate(-60)')
				.attr('text-anchor','end')
				.attr('x',-opt.tickLabelMargin)
				.text( function (d) { return (100- v); } )
				.classed('b-axis tick-text', true);

		axes.append('g')
			.attr('transform',function(d){
				return 'translate(' + coord3[0] + ',' + coord3[1] + ')'
			})
			.append("text")
				.text( function (d) { return v; } )
				.attr('x',opt.tickLabelMargin)
				.classed('c-axis tick-text', true);

	})

	function coord(arr){
		var a = arr[0], b=arr[1], c=arr[2]; 
		var sum, pos = [0,0];
	    sum = a + b + c;
	    if(sum !== 0) {
		    a /= sum;
		    b /= sum;
		    c /= sum;
			pos[0] =  corners[0][0] * a + corners[1][0]  * b + corners[2][0]  * c;
			pos[1] =  corners[0][1] * a + corners[1][1]  * b + corners[2][1]  * c;
		}
	    return pos;
	}
	function make_line(p1,p2,start_dist_os,end_dist_os){
		var dx = p2[0]-p1[0];
		var dy = p2[1]-p1[1];
		var l = Math.sqrt(dx*dx + dy*dy);
		if (l==0) {
			//can't add dist os with no length
			return { 
				x1:p1[0] , y1:p1[1] ,
				x2:p2[0] , y2:p2[1]  };
		} else{
			return { 
				x1:p1[0] + start_dist_os*dx/l, y1:p1[1] + start_dist_os*dy/l,
				x2:p2[0] +   end_dist_os*dx/l, y2:p2[1] +   end_dist_os*dy/l};
			}
		}


	function scale(p, factor) {
	    return [p[0] * factor, p[1] * factor];
	}

	plot.data = function(data, accessor, bindBy){ //bind by is the dataset property used as an id for the join
		plot.dataset = data;

		var circles = svg.selectAll("circle")
			.data( data.map( function(d){return [coord(accessor(d)),d.label,d.url,d.offset]; }), function(d,i){
				if(bindBy){
					return plot.dataset[i][bindBy];
				}
				return i;
			} );
		var nodeEnter = circles.enter().append("svg:a")
			.attr("xlink:href", function(d){ return d[2];});
		nodeEnter.append("circle")
			.attr("cx", function(d){return d[0][0];})
			.attr("cy", function(d){return d[0][1];})
			.attr("r", opt.circle_radius);
		nodeEnter.append("text")
			.attr("id", "data_label")
			.attr("dx", function(d){return d[0][0] + d[3][0];})
			.attr("dy", function(d){return d[0][1]-10 + d[3][1];})
			.text(function(d){return d[1];});

		return this;
	}

	plot.getPosition = coord;

	return plot;
}