//make a stacked line graph from data
//following chart API, except using selection as input: https://bost.ocks.org/mike/chart/
function pieChart() {
  var width = 500, // default width
      height = 500, // default height
      outer_radius = Math.min(width, height) / 2,
      inner_radius = outer_radius/2,
      title = '';
      //margin = {top: 30, right: 20, bottom: 30, left: 50},
      color = d3.scale.category20();
      //color = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]),
      //color = d3.scale.ordinal().range(["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"]);

      slice_value = function(d){return d[0]},
      label_value = function(d){return d[0]},
      selection_string = '',
      data = []; //data

  function my() {
    var arc = d3.svg.arc()
        .outerRadius(outer_radius-10)
        .innerRadius(inner_radius-10);
    var labelArc = d3.svg.arc()
        .outerRadius(outer_radius+10 )
        .innerRadius(outer_radius+10 );
    var pie = d3.layout.pie()
        .sort(null)
        .value(slice_value);
    var svg = d3.select(selection_string).append("svg")
      .attr("id","pie")
      .attr("width", width)
      .attr("height", height+16)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + (16+height / 2) + ")");

    var g = svg.selectAll(".arc")
        .data(pie(data))
      .enter().append("g")
        .attr("class", "arc");

    g.append("path")
        .attr("d", arc)
        .style("fill", function(d,i) { return color(i); });

    g.append("text")
        .attr("transform", function(d) {
          var pos = labelArc.centroid(d);
          pos[0] = (outer_radius+10) * (midAngle(d) < Math.PI ? 1 : -1);
          return "translate(" + pos + ")"; 
        })
        .attr("dy", ".35em")
        .attr("text-anchor",function(d){
          return midAngle(d) < Math.PI ? "start":"end";          
        })
        .text( label_value )

      function midAngle(d){
        return d.startAngle + (d.endAngle - d.startAngle)/2;
      }

    var polyline = svg.selectAll("polyline")
      .data(pie(data), label_value);
    polyline.enter()
      .append("polyline")
      .style("fill","none")
      .style("stroke","black")
      .style("opacity",.3)
      .style("stroke-width","2px");
    polyline.attr("points",function(d){
      var pos = labelArc.centroid(d);
      pos[0] = (outer_radius+10) * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
      return [arc.centroid(d), labelArc.centroid(d), pos];
    });



    g.selectAll("text")
      .call(my.wrap, 125)

    if (title != ''){
      svg.append("text")
              .attr("x", 0)             
              .attr("y", -height/2)
              .attr("text-anchor", "middle")  
              .style("font-size", "16px") 
              .text(title);
    }
  }

  my.wrap = function (text, width) {
    text.each(function() {
      var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = .5, // ems
          y = text.attr("y"),
          dy = parseFloat(text.attr("dy")),
          tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
      while (word = words.pop()) {
        line.push(word);
        tspan.text(line.join(" "));
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
          tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
        }
      }
    });
  }


  my.width = function(value) {
    if (!arguments.length) return width;
    width = value;
    outer_radius = Math.min(width, height) / 2-20;
    inner_radius = outer_radius/2;
    return my;
  };
  my.height = function(value) {
    if (!arguments.length) return height;
    height = value;
    outer_radius = Math.min(width, height) / 2-20 ;
    inner_radius = outer_radius/2;
    return my;
  };
  my.inner_radius = function(value) {
    if (!arguments.length) return inner_radius;
    inner_radius = value;
    return my;
  };
  my.outer_radius = function(value) {
    if (!arguments.length) return outer_radius;
    outer_radius = value;
    return my;
  };
//  my.margin = function(value) {
//    if (!arguments.length) return margin;
//    margin = value;
//    return my;
//  };
  my.color = function(value) {
    if (!arguments.length) return color;
    color = value;
    return my;
  };
  my.slice_value = function(value) {
    if (!arguments.length) return slice_value;
    slice_value = value;
    return my;
  };
  my.label_value = function(value) {
    if (!arguments.length) return label_value;
    label_value = value;
    return my;
  };
  my.selection_string = function(value) {
    if (!arguments.length) return selection_string;
    selection_string = value;
    return my;
  };
  my.data = function(value) {
    if (!arguments.length) return data;
    data = value;
    return my;
  };
  my.title = function(value) {
    if (!arguments.length) return title;
    title = value;
    return my;
  };

  return my;
}