//curve fitting routines
//requires math.js

function linear_least_squares(phis,xs,ys){
	//return beta_j that minimizes sum_i(f(x_i)-y_i)**2 where f=sum_j beta_j phi_j
	//see https://en.wikipedia.org/wiki/Least_squares
	//first evaluate basis functions at data points
	var X = [];
	for(var i=0; i<xs.length; i++){
		var X_i = [];
		for(var j=0; j<phis.length; j++){
			X_i.push( phis[j](xs[i]) );
		}
		X.push(X_i);
	}
	//then solve linear system
	return math.lusolve( math.multiply( math.transpose(X), X ),  math.multiply( math.transpose(X), ys ));
}

function linear_fit(xs,ys){
	// f = beta[0]*x + beta[1]
	return linear_least_squares([function(x){return x;}, function(x){return 1;}],xs,ys);
}
function homogeneous_quadratic_fit(xs,ys){
	// f = beta[0] * x^2
	return linear_least_squares([function(x){return x*x;}],xs,ys);
}
function exponential_fit(xs,ys){
	//f = beta[0] * e^(beta[1]*x)
	var beta = linear_least_squares([function(x){return 1;}, function(x){return x;} ], xs, ys.map(Math.log));
	beta[0] = Math.exp(beta[0]);
	return beta
}
function power_fit(xs,ys){
	//f = beta[0] * x ^ beta[1]
	var beta = linear_least_squares([function(x){return 1;}, function(x){return Math.log(x);} ], xs, ys.map(Math.log) );
	beta[0] = Math.exp(beta[0]);
	return beta;
}

