//requires d3.pie-chart.js
function run_scoreboard(key){
	//put up scoreboard
	$.getJSON("../results.json", function(values){
		var values = values[key];
		values.name = key;
		var scoreboard = document.createElement('span');
		scoreboard.id = 'scoreboard';
		var template = "<div style='float:left'>\
						<h2>{{name}}</h2>\
						<p>\
						<b>Materials Energy</b>: {{materials_energy}}<br>\
						<b>Manufacturing Energy</b>: {{manufacturing_energy}}<br>\
						<b>Use Energy</b>: {{use_energy}}<br>\
						<b>Energy unit</b>: {{energy_unit}}<br>\
						<b>Product unit</b>: {{product_unit}}<br>\
						</p></div>"
		scoreboard.innerHTML = Mustache.to_html(template,values);
		document.body.insertBefore(scoreboard,document.body.childNodes[0]);

		//pie chart
		var data = [
			{'energy':values.materials_energy, 'name':'Materials', 'energy_unit':values.energy_unit,'product_unit':values.product_unit},
			{'energy':values.manufacturing_energy, 'name':'Manufacturing', 'energy_unit':values.energy_unit,'product_unit':values.product_unit},
			{'energy':values.use_energy, 'name':'Use', 'energy_unit':values.energy_unit,'product_unit':values.product_unit},
		]
	    chart = pieChart()
			.data(data)
			.selection_string("#scoreboard")
			.width(600).height(300)
			.title('')
			.slice_value(function(d) { return d.energy; })
			.label_value(function(d) { return d.data.name + ' ' + d.data.energy + ' ' + d.data.energy_unit + '/' + d.data.product_unit; });
		chart();
    	
	});
}

